﻿namespace XO
{
    internal class Program
    {
        static string[,] gamePoleMap = new string[3, 3];

        static void DefaultValueGame()
        {
            int count = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    count++;
                    gamePoleMap[i, j] = count.ToString();
                }
            }
        }

        static void PCStep()
        {
            Random rand = new Random();
            int indexStepI = rand.Next(0, 8);
            int indexStepJ = rand.Next(0, 8);
            bool check = Check(indexStepI, indexStepJ);
            if (check)
            {
                gamePoleMap[indexStepI, indexStepJ] = "X";
            }
            else
                PCStep();
        }

        static bool Check(int indexStepI, int indexStepJ)
        {
            if (gamePoleMap[indexStepI, indexStepJ] != "X" && gamePoleMap[indexStepI, indexStepJ] != "O")
            {
                return true;
            }
            return false;
        }

        static bool IsEmpty()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gamePoleMap[i, j] != "X" && gamePoleMap[i, j] != "O")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        static bool GetWinner(string figure)
        {
            int hor;
            int vert;

            for (int i = 0; i < 3; i++)
            {
                hor = 0;
                vert = 0;
                for (int j = 0; j < 3; j++)
                {
                    if (gamePoleMap[i, j] == figure)
                    {
                        hor++;
                    }
                    if (gamePoleMap[j, i] == figure)
                    {
                        vert++;
                    }
                }
                if (hor == 3 || vert == 3)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            DefaultValueGame();
            GetWinner("X");
        }
    }
}